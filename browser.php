<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="browser.css">
	<title>Directory Contents</title>

<script>
function open_form(id) {
	document.getElementById("form_action_" + id).style.display = "block";
	document.getElementById("open_action_" + id).style.display = "none";
}

function close_form(id) {
	document.getElementById("form_action_" + id).style.display = "none";
	document.getElementById("open_action_" + id).style.display = "block";
}
</script>
</head>
<body>
<?php 

/********** CONFIG **********/
define("THUMB_FOLDER","thumbs");
define("FOLDER_CEATE_PREMISSION",0777);
define("FILE_CEATE_PREMISSION",0777);
/* image extensions to detect (lower case) */
$exts_image = array("png", "jpg", "jpeg", "svg", "gif", "ico");
/* text extensions to detect (lower case) */
$exts_editable = array("txt", "log", "htm", "html", "xhtml", "shtml", "php", "js", "css", "md", "csv", "xml", "json");
/* document extensions to detect (lower case) */
$exts_document = array("pdf", "xls", "xlsx", "doc", "docx");
/* extensions to hide (if not admin) */
$exts_hidden = array("php", "htaccess");
$is_admin = true;
/********** CONFIG **********/

/********** EXTRACT GET/POST DATA **********/
/* Extract current directory from the GET */
if("" != $_GET['folder']) {
	$baseDir = str_replace(basename($_SERVER["SCRIPT_FILENAME"]),"",$_SERVER["SCRIPT_FILENAME"]);
	if(substr($_GET['folder'],0,1) == ".." || substr($_GET['folder'],0,1) == "/" || strpos($_GET['folder'],"..") > -1) {
		$current_dir = "./";
		echo "<font color='red'>Access to ".$_GET['folder']." is forbiden!</font><br>";
	}
	else {
		$current_dir = $current_dir.$_GET['folder']."/";
	}
}
else {
	$current_dir = "./";
}
/* the admin can create folders, upload files and perform actions on file */
if(true == $is_admin) {
	/* create new folder */
	if($_POST["new_folder_name"] != "") {
		if(!file_exists($current_dir.$_POST["new_folder_name"]))
		{
			mkdir($current_dir.$_POST["new_folder_name"],0777);
		}
	}
	/* upload multiple files */
	if (isset($_FILES['files_to_upload'])) {
		$my_file = $_FILES['files_to_upload'];
		$file_count = count($my_file["name"]);

		for ($i = 0; $i < $file_count; $i++) {
			/*echo "File #".$i.":<br>";
			echo "Name: ".$my_file['name'][$i]."<br>";
			echo "Temporary file: ".$my_file['tmp_name'][$i]."<br>";
			echo "Type: ".$my_file['type'][$i]."<br>";
			echo "Size: ".$my_file['size'][$i]."<br>";
			echo "Error: ".$my_file['error'][$i]."<br>";*/
			if($my_file['name'][$i] != "") {
				if(!move_uploaded_file($my_file['tmp_name'][$i],$current_dir.basename($my_file['name'][$i]))) {
					echo "Failed to upload '".basename($my_file['name'][$i])."'<br>";
				}
				chmod($current_dir.basename($my_file['name'][$i]),FILE_CEATE_PREMISSION);
			}
		}
	}
	/* old file upload handling which handle only a single file */
	if($_FILES['file_to_upload']['name'] != "") {
		if(!move_uploaded_file($_FILES['file_to_upload']['tmp_name'],$current_dir.basename($_FILES['file_to_upload']['name']))) {
			echo "Failed to upload '".basename($_FILES['file_to_upload']['name'])."'<br>";
		}
		chmod($current_dir.basename($_FILES['file_to_upload']['name']),FILE_CEATE_PREMISSION);
	}
	/* delete the given file */
	if($_POST["action"] == "delete") {
		//unlink($current_dir."/".THUMB_FOLDER."/".THUMB_DIAGONAL."_".$_GET["file"]);
		//unlink($current_dir."/".THUMB_FOLDER."/".VIEW_DIAGONAL."_".$_GET["file"]);
		unlink($_GET["file"]);
	}
}
/********** EXTRACT GET/POST DATA **********/
?>

<div id='mainContent'>
<h1>Directory Contents</h1>

<?php
/*$parent_dir = get_parent_directory($current_dir);
if($parent_dir == "") {
	//echo "<div class='upload-btn-wrapper'><button type=\"submit\" formaction=\"".$_SERVER["PHP_SELF"]."\">Up One Directory</button></div></div>";
	echo "<div class='upload-btn-wrapper'><div class='btn'><a href='".$_SERVER["PHP_SELF"]."'>Up One Directory</a></div></div>";
}
else {
	//echo "<div class='upload-btn-wrapper'><button type=\"submit\" formaction=\"".$_SERVER["PHP_SELF"]."?folder=".$parent_dir."\">Up One Directory</button></div>";
	echo "<div class='upload-btn-wrapper'><div class='btn'><a href='".$_SERVER["PHP_SELF"]."?folder=".$parent_dir."'>Up One Directory</a></div></div>";
}*/

if($is_admin) {
?>
	<form id="main" name="main" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>" method="post" enctype="multipart/form-data">
	<!-- Upload file -->
	<!--<div class="upload-btn-wrapper"><button class="btn">Select file</button><input type="file" id="file_to_upload" name="file_to_upload" /></div>-->
	<div class="upload-btn-wrapper"><button>Select file(s)</button><input type="file" id="file_to_upload" name="files_to_upload[]" multiple /></div>
	<div class="upload-btn-wrapper"><button type="submit" name="submit" id="submit" value="Upload">Upload</button></div>
	<!-- Create folder -->
	<div class="upload-btn-wrapper">
	<input class="holo" type="text" id="new_folder_name" name="new_folder_name" onkeyup="this.value=this.value.replace(/\s|\./g,'');" /> 
	<button type="submit" name="submit" id="submit" value="Add Folder">New folder</button>
	</div>
</form>

 <?php
}
/* Add pretty file size */
function pretty_filesize($file) {
	$size=filesize($file);
	if($size<1024) $size=$size." Bytes";
	elseif($size<1048576) $size=round($size/1024, 1)." KB";
	elseif($size<1073741824) $size=round($size/1048576, 1)." MB";
	else $size=round($size/1073741824, 1)." GB";
	return $size;
}
?>
	<table class="sortable">
		<thead>
		<tr>
			<th class="left" style="width: 70%">Filename</th>
			<th style="width: 15%">Size</th>
			<th style="width: 15%">Actions</th>
		</tr>
		</thead>
		<tbody>
<?php

$parent_dir = get_parent_directory($current_dir);
if("./" != $current_dir) {
	if("" == $parent_dir) {
		echo("
		<tr class='$class'>
			<td class=\"left\"><a href='".$_SERVER["PHP_SELF"]."' class='name'>&#11176; Go back</a></td>
			<td></td>
			<td></td>
		</tr>");
	}
	else {
		echo("
		<tr class='$class'>
			<td class=\"left\"><a href='".$_SERVER["PHP_SELF"]."?folder=".$parent_dir."' class='name'>&#11176; Go back</a></td>
			<td></td>
			<td></td>
		</tr>");
	}
}

/* The variable $is_dir should be set to true to extract directories or to
 * false to extract files */
function get_files_or_directories($base_path, $array, $is_dir) {
	$list_elements = array();
	$array_size = count($array);
	for($index=0; $index < $array_size; $index++) {
		if($is_dir == is_dir($base_path.$array[$index])) {
			array_push($list_elements, $array[$index]);
		}
	}
	sort($list_elements);
	return $list_elements;
}

/* Opens directory */
$working_dir = opendir($current_dir);
/* Gets each entry */
while($entry_name = readdir($working_dir)) {
	$filename_array[] = $entry_name;
}
/* Closes directory */
closedir($working_dir);

$list_dirs = get_files_or_directories($current_dir, $filename_array, true);
$list_files = get_files_or_directories($current_dir, $filename_array, false);

/* Counts elements in array */
$index_count = count($list_dirs);

/* Loops through the array of directories */
for($index=0; $index < $index_count; $index++) {
	if(substr("$list_dirs[$index]", 0, 1)!=".") {
		// Gets File Names
		$name = $list_dirs[$index];
		$path = $current_dir.$list_dirs[$index];

		// Gets Date Modified
		//$modtime=date("M j Y g:i A", filemtime($filename_array[$index]));
		//$timekey=date("YmdHis", filemtime($filename_array[$index]));

		// Separates directories, and performs operations on those directories
		$class="dir";
		$size="&lt;Directory&gt;";
		$sizekey="0";

		// Cleans up . and .. directories
		if($name == "." || $name == "..") {
			$name = ". (Current Directory)";
			$extn = "&lt;System Dir&gt;";
		}

		$href = "".$_SERVER['PHP_SELF']."?folder=".urlencode($path);
		// Output
		echo("
		<tr class='$class'>
			<td class=\"left\"><a href='$href' class='name'>&#128193; $name</a></td>
			<td sorttable_customkey='$sizekey'><a href='$href'>$size</a></td>
			<td sorttable_customkey='$timekey'><a href='$href'>$modtime</a></td>
		</tr>");
	}
}

/* Counts elements in array */
$index_count = count($list_files);

/* Loops through the array of files */
for($index=0; $index < $index_count; $index++) {
	// Decides if hidden files should be displayed, based on query above.
	if(substr("$list_files[$index]", 0, 1)!=".") {
		// Gets File Names
		$name = $list_files[$index];
		$path = $current_dir.$list_files[$index];

		$class="file";
		/* Gets file extension */
		$extn = pathinfo($path, PATHINFO_EXTENSION);
		$is_editable = false;
		$is_document = false;
		$is_image = false;

		/* images */
		if(in_array(strtolower($extn), $exts_image)) {
			$is_image = true;
		}
		/* editable files */
		elseif(in_array(strtolower($extn), $exts_editable)) {
			$is_editable = true;
		}
		/* documents */
		elseif(in_array(strtolower($extn), $exts_document)) {
			$is_document = true;
		}
		/* others */
		else {
		}

		if((false == $is_admin) && (in_array(strtolower($extn), $exts_hidden))) {
			/* if not admin and ext is hidden, go to next loop iteration */
			continue;
		}

		/* Gets and cleans up file size */
		$size = pretty_filesize($path);
		$sizekey = filesize($path);

		$namehref = $path;
		/* remove the inital "./" to build a working link */
		if(".%2F" == substr($namehref, 0, 4)) $namehref = substr($namehref, 4);

		/* build link for actions (edit/download) and set icon */
		$form_action = "";
		$form_action = "<div><form>";
		//$form_action .= "<button class=\"action\" id=\"open_action_$index\" onclick=\"open_form($index)\">&#10133;</button>";
		//$form_action .= "<div class=\"form-popup\" style=\"display: none;\" id=\"form_action_$index\">";
		//$form_action .= "<form class=\"action-form-container\">";
		//$form_action .= "<h2>$name</h2><hr>";
		if($is_editable) {
			if($is_admin) $form_action .= "<button class=\"action\" type=\"submit\" formmethod=\"post\" formaction=\"editor.php?file=$namehref\" name=\"action\" value=\"edit\">&#128393;</button>";
		}
		$form_action .= "<button class=\"action\" type=\"submit\" formmethod=\"post\" formaction=\"$namehref\" name=\"action\" value=\"download\">&#128190;</button>";
		if($is_admin) $form_action .= "<button class=\"action\" type=\"submit\" formmethod=\"post\" formaction=\"".$_SERVER['PHP_SELF']."?folder=".substr($current_dir, 0, -1)."&file=$path\" name=\"action\" value=\"rename\">&#128256;</button>";
		if($is_admin) $form_action .= "<button class=\"action\" type=\"submit\" formmethod=\"post\" formaction=\"".$_SERVER['PHP_SELF']."?folder=".substr($current_dir, 0, -1)."&file=$path\" name=\"action\" value=\"delete\" onclick=\"return confirm('Do you want to delete $name?');\">&#10008;</button>";
		//$form_action .= "<button class=\"action\" type=\"button\" onclick=\"close_form($index)\">&#10134;</button>";
		//$form_action .= "</form></div>";

		$icon = "&#128459;";
		if($is_editable) $icon = "&#128462;";
		elseif($is_document) $icon = "&#128442;";
		elseif($is_image) $icon = "&#128247;";
		
		// Output
		echo("
		<tr class='$class'>
			<td class=\"left\"><a href='$namehref' class='name'>$icon $name</a></td>
			<td sorttable_customkey='$sizekey'><a href='$namehref'>$size</a></td>
			<td>$form_action</td>
		</tr>");
	}
}
?>
		</tbody>
	</table>

<!-- Page footer -->

<?php
function get_parent_directory($current_dir)
{
	/* remove last slash if present */
	while("/" == substr($current_dir, -1)) $current_dir = substr($current_dir, 0, -1);
	$parent = strrchr($current_dir,"/");
	$parent = str_replace($parent,"",$current_dir);
	//$lastSlash = substr($lastSlash,1,strlen($lastSlash));
	if(!$parent || ($parent == ".")) {
		$parent = "";
	}
	return $parent;
}
?>
</body>
</html>