# PhpBrowser

Php script to browse files on the server

## Installation

Copy the 3 files (browser.css, browser.php and editor.php) at the location you
want to browser files. The browser will allow you to browse children but not the
parent directory of the browser.php location.

## Functionalities

- Directories
    - [x] List files and directories
    - [x] Go to parent directory
    - [x] Browse child directories
    - [x] Create a directory
    - [ ] Delete a directory
    - [ ] Rename a directory

- Files
    - [x] Download a file
    - [x] Upload files
    - [x] Delete a file
    - [x] Edit text files
    - [ ] Rename a file

- Pictures
    - [ ] Create thumbnails