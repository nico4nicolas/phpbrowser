<?php

// configuration
//$url = 'http://domain.com/backend/editor.php';
list($url) = get_included_files();
$file = $_GET['file'];

// check if form has been submitted
if (isset($_POST['text']))
{
	// save the text contents
	file_put_contents($file, $_POST['text']);

	// redirect to form again
	header(sprintf('Location: %s', $url));
	printf('<a href="%s">Moved</a>.', htmlspecialchars($url));
	exit();
}

// read the textfile
$text = file_get_contents($file);

?>
<!-- HTML form -->
<form action="" method="post">
	<textarea style="width: 98%; height: 50%; border: 3px solid #cccccc; padding: 5px; margin: 1%;" name="text"><?php echo htmlspecialchars($text) ?></textarea>
	<input type="hidden" value="<?php echo $file ?>"/>
	<input type="submit" value="Save"/>
	<input type="reset" value="Reset"/>
</form>